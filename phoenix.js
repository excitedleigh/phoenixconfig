// Work Mode
const TERMINAL_APPS = new Set(['com.apple.Terminal', 'com.apple.Console', 'com.googlecode.iterm2'])
const EDITOR_APPS = new Set(['com.sublimetext.3', 'com.microsoft.VSCode'])
const BROWSER_APPS = new Set([
    'org.mozilla.firefox',
    'com.apple.Safari',
    'com.google.Chrome',
    'com.operasoftware.Opera',
    'com.vivaldi.Vivaldi',
])
const DOC_APPS = new Set(['com.kapeli.dashdoc'])

var workModeOn = false
var workSpaces = new Set()

function activateWorkMode() {
    //workModeOn = !workModeOn
    //if (!workModeOn) return
    workSpaces = new Set(Screen.all().map(x => x.currentSpace()))
    layoutWindows()
}

function splitRectangle(rect, distance, from) {
    if (from === 'left') {
        return [
            Object.assign({}, rect, { width: distance }),
            Object.assign({}, rect, { x: rect.x + distance, width: rect.width - distance }),
        ]
    } else if (from === 'right') {
        return [
            Object.assign({}, rect, { width: rect.width - distance }),
            Object.assign({}, rect, { x: rect.x + rect.width - distance, width: distance })
        ]
    } else if (from === 'top') {
        return [
            Object.assign({}, rect, { height: distance }),
            Object.assign({}, rect, { y: rect.y + distance, height: rect.height - distance }),
        ]
    } else if (from === 'bottom') {
        return [
            Object.assign({}, rect, { height: rect.height - distance }),
            Object.assign({}, rect, { y: rect.y + rect.height - distance, height: distance }),
        ]
    } else {
        Phoenix.log(`ERROR: Don't know how to split from ${from}`)
    }
}

function splitEvenly(rect, times, direction) {
    if (direction == 'longest') {
        if (rect.width > rect.height) return splitEvenly(rect, times, 'horizontal')
        else return splitEvenly(rect, times, 'vertical')
    } else if (direction == 'vertical') {
        return _.range(times).map(
            x => Object.assign({}, rect, { height: rect.height / times, y: rect.y + (rect.height / times * x) })
        )
    } else if (direction == 'horizontal') {
        return _.range(times).map(
            x => Object.assign({}, rect, { width: rect.width / times, x: rect.x + (rect.width / times * x) })
        )
    }
}

function arrangeWindowsEvenly(rect, windows, direction) {
    const rects = splitEvenly(rect, windows.length, direction)
    _.zip(windows, rects).forEach(([window, rect]) => window.setFrame(rect))
}

function layoutWindows() {
    // Collect all windows into categories
    const terminalWindows = []
    const editorWindows = []
    const browserWindows = []
    const docWindows = []
    const miscWindows = []
    for (space of workSpaces) {
        for (window of space.windows()) {
            const bid = window.app().bundleIdentifier()
            if (bid === 'com.apple.loginwindow') continue
            else if (TERMINAL_APPS.has(bid)) {
                terminalWindows.push(window)
            }
            else if (EDITOR_APPS.has(bid)) {
                editorWindows.push(window)
            }
            else if (BROWSER_APPS.has(bid)) {
                browserWindows.push(window)
            }
            else if (DOC_APPS.has(bid)) {
                docWindows.push(window)
            }
            else {
                miscWindows.push(window)
                Phoenix.log(`Misc window of app ${bid}`)
            }
        }
    }

    const mainScreen = Screen.main()
    const otherScreens = Screen.all().filter(x => !x.isEqual(mainScreen))

    if (otherScreens.length == 1) {
        const otherScreen = otherScreens[0]
        const [terminalArea, mainArea] = splitRectangle(mainScreen.flippedVisibleFrame(), 640, 'left')
        const [editorArea, readingArea] = splitRectangle(mainArea, 1160, 'right')
        const [browserArea, docArea] = splitRectangle(readingArea, 600, 'bottom')
        arrangeWindowsEvenly(terminalArea, terminalWindows, 'vertical')
        arrangeWindowsEvenly(editorArea, editorWindows, 'vertical')
        arrangeWindowsEvenly(browserArea, browserWindows, 'vertical')
        arrangeWindowsEvenly(docArea, docWindows, 'horizontal')
        arrangeWindowsEvenly(otherScreen.flippedVisibleFrame(), miscWindows, 'horizontal')
    } else if (otherScreens.length == 2) {
        var terminalScreen, otherScreen
        const firstOtherScreenFrame = otherScreens[0].flippedVisibleFrame()
        const secondOtherScreenFrame = otherScreens[1].flippedVisibleFrame()
        if (firstOtherScreenFrame.width < firstOtherScreenFrame.height) {
            terminalScreen = otherScreens[0]
            otherScreen = otherScreens[1]
        } else if (secondOtherScreenFrame.width < secondOtherScreenFrame.height) {
            terminalScreen = otherScreens[1]
            otherScreen = otherScreens[0]
        } else if (firstOtherScreenFrame.width * firstOtherScreenFrame.height < secondOtherScreenFrame.width * secondOtherScreenFrame.height) {
            terminalScreen = otherScreens[0]
            otherScreen = otherScreens[1]
        } else {
            terminalScreen = otherScreens[1]
            otherScreen = otherScreens[0]
        }
        const readingAreaWidth = Math.min(1160, mainScreen.flippedVisibleFrame().width / 2)
        const [editorArea, readingArea] = splitRectangle(mainScreen.flippedVisibleFrame(), readingAreaWidth, 'right')
        const docAreaHeight = Math.min(600, readingArea.height / 3)
        const [browserArea, docArea] = splitRectangle(readingArea, docAreaHeight, 'bottom')
        arrangeWindowsEvenly(terminalScreen.flippedVisibleFrame(), terminalWindows, 'longest')
        arrangeWindowsEvenly(editorArea, editorWindows, 'vertical')
        arrangeWindowsEvenly(browserArea, browserWindows, 'vertical')
        arrangeWindowsEvenly(docArea, docWindows, 'horizontal')
        arrangeWindowsEvenly(otherScreen.flippedVisibleFrame(), miscWindows, 'horizontal')
    } else {
        Phoenix.log(`otherScreens.length = ${otherScreens.length} ${otherScreens}`)
    }
}

Key.on('W', ['ctrl', 'cmd', 'alt'], activateWorkMode)
//activateWorkMode()

Key.on('I', ['ctrl', 'cmd', 'alt'], () => {
    Phoenix.log("Info!")
    const bundleId = App.focused().bundleIdentifier()
    const modal = Modal.build({
        text: bundleId,
        duration: 5,
        origin: frame => ({ x: 500, y: 500 }),
    })
    modal.show()
})